Rails.application.routes.draw do
	resources :subjects do 
		member do 
			get :delete
		end 
	end 
  # get 'subjects/index'

  # get 'subjects/show'

  # get 'subjects/new'

  # get 'subjects/edit'

  # get 'subjects/delete'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
